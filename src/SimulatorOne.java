import java.util.*;

public class SimulatorOne {

  public static void main(String[] args) {

    Scanner s = new Scanner(System.in);
    ArrayList<String> al = new ArrayList<>();
    al.add(s.nextLine());
    int num = Integer.parseInt(al.get(0)) + 4;

    for (int i = 0; i < num; i++) {
      al.add(s.nextLine());
      //System.out.println(al.toString());
    }
    Read rd = new Read(al);
    SimulatorOne sim = new SimulatorOne();
    DiGraph gr = sim.buildGraph(rd);

    ArrayList<Integer> clients = rd.getClients();

    for (int i = 0; i < clients.size(); i++) {
      List<List<String>> taxiPaths = new ArrayList<List<String>>();
      List<List<String>> shopPaths = new ArrayList<List<String>>();
      System.out.println("client " + clients.get(i));
      try {
        taxiPaths = sim.findTaxi(gr, rd, Integer.toString(clients.get(i)));
        shopPaths = sim.findShop(gr, rd, Integer.toString(clients.get(i)));
        sim.output(Integer.toString(clients.get(i)), taxiPaths, shopPaths);
      } catch (NullPointerException | IndexOutOfBoundsException e) {
        System.out.println("cannot be helped");
      }
    }
  }

  public DiGraph buildGraph(Read rd) {

    // add root plus Graphs
    DiGraph graph = new DiGraph();
    ArrayList<Integer>[] edges = rd.getEdges();
    int numOfNodes = edges.length;
    for (int i = 0; i < numOfNodes; i++) {
      for (int j = 1; j < edges[i].size(); j = j+2) {
        graph.add(Integer.toString(edges[i].get(0)), Integer.toString(edges[i].get(j)), edges[i].get(j+1));
      }
    }

    return graph;
  }
  /**
  * Finds the nearest taxi to a client.
  *
  *
  */
  public List<List<String>> findTaxi(DiGraph graph, Read rd, String client) {

    int num = rd.getNumShops();
    ArrayList<Integer> shops = rd.getShops();
    List<String> shortestpath = new ArrayList<String>();
    List<List<String>> paths = new ArrayList<List<String>>();
    int minweight = 999999;

    for (int i = 0; i < num; i++) {
      List<String> path = graph.getPath(Integer.toString(shops.get(i)), client);
      String last = path.get(path.size()-1);
      String[] lastArray = last.split(" : ");
      int weight = 9999999;
      if (!(lastArray[lastArray.length - 1].equals("No path found"))) {
        weight = Integer.parseInt(lastArray[lastArray.length - 1]);
      }

      if (!path.isEmpty()) {

        if (weight < minweight) {
          minweight = weight;
          shortestpath = path;
        }

      }
    }

    paths.add(shortestpath);

    for (int i = 0; i < num; i++) {

      List<String> path = graph.getPath(Integer.toString(shops.get(i)), client);
      String last = path.get(path.size()-1);
      String[] lastArray = last.split(" : ");
      int weight = 9999999;
      if (!(lastArray[lastArray.length - 1].equals("No path found"))) {
        weight = Integer.parseInt(lastArray[lastArray.length - 1]);
      }

      if (!path.isEmpty()) {

        if (weight == minweight && !(path.equals(shortestpath))) {
          paths.add(path);
        }
      }
    }
    return paths;
  }

  public List<List<String>> findShop(DiGraph graph, Read rd, String client) {
    int num = rd.getNumShops();
    ArrayList<Integer> shops = rd.getShops();
    List<String> shortestpath = new ArrayList<String>();
    List<List<String>> paths = new ArrayList<List<String>>();
    int minweight = 99999;

    for (int i = 0; i < num; i++) {
      List<String> path = graph.getPath(client, Integer.toString(shops.get(i)));
      String last = path.get(path.size()-1);
      String[] lastArray = last.split(" : ");
      int weight = 9999999;
      if (!(lastArray[lastArray.length - 1].equals("No path found"))) {
        weight = Integer.parseInt(lastArray[lastArray.length - 1]);
      }


      if (!path.isEmpty()) {

        if (weight < minweight) {
          minweight = weight;
          shortestpath = path;
        }
      }
    } // min weight found
    paths.add(shortestpath);

    for (int i = 0; i < num; i++) {
      List<String> path = graph.getPath(client, Integer.toString(shops.get(i)));
      String last = path.get(path.size()-1);
      String[] lastArray = last.split(" : ");
      int weight = 9999999;
      if (!(lastArray[lastArray.length - 1].equals("No path found"))) {
        weight = Integer.parseInt(lastArray[lastArray.length - 1]);
      }

      if (!path.isEmpty()) {

        if (weight == minweight && !(path.equals(shortestpath))) {
          paths.add(path);
        }

      }
    }

    return paths;

  }

  public void output(String client, List<List<String>> taxiPaths, List<List<String>> shopPaths) {
    
    try {
      for (List<String> each : taxiPaths) {
        String taxi = each.get(0); //"node : weight"
        taxi = taxi.split(" : ")[0]; // "node"
        List<String> taxiPath = pathToString(each);
        String taxiPathString = "";
        for (int i = 0; i < taxiPath.size(); i++) {
          taxiPathString = taxiPathString + taxiPath.get(i) + " ";
        }
      }
      for (List<String> each : shopPaths) {
        List<String> shopPath = pathToString(each);
        String shop = each.get(each.size() - 1);
        shop = shop.split(" : ")[0];
        String shopPathString = "";
        for (int i = 0; i < shopPath.size(); i++) {
          shopPathString = shopPathString + shopPath.get(i) + " ";
        }
      }
    } catch(NullPointerException | IndexOutOfBoundsException e) {
      System.out.println("cannot be helped");
      return;
    }

    for (List<String> each : taxiPaths) {
      String taxi = each.get(0); //"node : weight"
      taxi = taxi.split(" : ")[0]; // "node"
      List<String> taxiPath = pathToString(each);
      String taxiPathString = "";
      for (int i = 0; i < taxiPath.size(); i++) {
        taxiPathString = taxiPathString + taxiPath.get(i) + " ";
      }
      System.out.println("taxi " + taxi);
      System.out.println(taxiPathString);
    }
    for (List<String> each : shopPaths) {
      List<String> shopPath = pathToString(each);
      String shop = each.get(each.size() - 1);
      shop = shop.split(" : ")[0];
      String shopPathString = "";
      for (int i = 0; i < shopPath.size(); i++) {
        shopPathString = shopPathString + shopPath.get(i) + " ";
      }
      System.out.println("shop " + shop);
      System.out.println(shopPathString.trim());
    }


  }
  /**
  *   Removes the weights from paths.
  */
  public List<String> pathToString(List<String> path) { //path = [node : cost, node : cost, ...]
    List<String> newPath = new ArrayList<String>();
    for (int i = 0; i < path.size(); i++) {
      String temp = path.get(i);
      temp = temp.split(" : ")[0];
      newPath.add(temp);
    }
    return newPath;
  }
}
