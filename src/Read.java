
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author Snehin
 */
public class Read {

    //String fileName, newLine;
    //int size, index = 0;
    ArrayList<String> lines;
    /**
     * The constructor that takes in the file location and number of rows to initialize variables in the class
     * @param lines
     *
     */
    public Read(ArrayList<String> lines) {
        this.lines = lines;
        // System.out.println(lines.toString());
    }

    public int getNumNodes() {
      return Integer.parseInt(lines.get(0));
    }

    public int getNumShops() {
      return Integer.parseInt(lines.get(getNumNodes() + 1));
    }

    public ArrayList<Integer> getShops() {

      Scanner s = new Scanner(lines.get(2 + getNumNodes()));
      ArrayList<Integer> al = new ArrayList<Integer>();
      while (s.hasNextInt()) {
        al.add(s.nextInt());
      }
      return al;
    }

    public int getNumClients() {
      return Integer.parseInt(lines.get(getNumNodes() + 3));
    }

    public ArrayList<Integer> getClients() {

      Scanner s = new Scanner(lines.get(4 + getNumNodes()));
      ArrayList<Integer> al = new ArrayList<Integer>();
      while (s.hasNextInt()) {
        al.add(s.nextInt());
      }
      return al;
    }

    /**
    * Returns Array Lists for each vertex and in numbered order where
    * ArrayList[0] is the edges for vertex 0 and al[0](0) is the vertex 0.
    */
    public ArrayList<Integer>[] getEdges() {

      ArrayList<Integer>[] al = (ArrayList<Integer>[])new ArrayList[getNumNodes()];

      for (int i = 1; i <= getNumNodes(); i++) {
        Scanner s = new Scanner(lines.get(i));
        al[i-1] = new ArrayList<Integer>();

        while (s.hasNextInt()) {
          al[i-1].add(s.nextInt());
        }
      }
      return al;
    }
  }
